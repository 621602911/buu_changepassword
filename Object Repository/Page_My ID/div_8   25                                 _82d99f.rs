<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_8   25                                 _82d99f</name>
   <tag></tag>
   <elementGuidId>bd273ff6-405b-47bf-ac08-45cf39a2fcb4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-12 > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        
            
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  '  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  '  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                    Change Password
                
            
        
    
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เปลี่ยนรหัสผ่าน (Change Password)'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
        
        
            
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                    Change Password
                
            
        
    
    
&quot;) or . = concat(&quot;
        
        
            
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                    Change Password
                
            
        
    
    
&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
