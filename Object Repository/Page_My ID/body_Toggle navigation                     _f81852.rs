<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>body_Toggle navigation                     _f81852</name>
   <tag></tag>
   <elementGuidId>55be2b7b-fa6b-4bb1-b642-bf5c6612d834</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


    

        
        
            
                
                    Toggle navigation
                    
                    
                    
                
                
            
            

            
                
				
                
                
              
          
            

            
                
                    
                    	                        
                   	 	
                        
                             ลงทะเบียนครั้งแรก      First Register
                        
                        
                        
                             เปลี่ยนรหัสผ่าน/ต่ออายุรหัสผ่าน      Change password/Renew password
                        
                        
                             กู้คืนรหัสผ่าน/ต่ออายุรหัสผ่าน      Recovery password/Renew password
                        
                        
                             เข้าสู่ระบบ
                                  Sign In
                        
						
                             ติดต่อเรา      Contact Us
                        
                        
                        
                        
                             คู่มือการใช้งาน      Manual
                        
                        
                             คู่มือ Digital Cert for Windows      
                        
                        
                             คู่มือ Digital Cert for Mac      
                        
                                            
                
                
            
            
        

        
            
                
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  '  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  '  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                    Change Password
                
            
        
    
    



function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  var z = document.getElementById(&quot;oldpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
	z.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
	z.type = &quot;password&quot;;
  }
}

function checkuser(event){
	var ew = event.which;
	if(ew == 45)
        return true;
	if(ew == 46)
        return true;
	if(ew == 32)
        return true;
    if(48 &lt;= ew &amp;&amp; ew &lt;= 57)
        return true;
    if(65 &lt;= ew &amp;&amp; ew &lt;= 90)
        return true;
    if(97 &lt;= ew &amp;&amp; ew &lt;= 122)
        return true;
    return false;
}

function checkold(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don't use ' (single quote) ห้ามใช้เครื่องหมาย ' (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                
                
                
            
        
        
    
    
    

    
    

    
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute(&quot;page_id&quot;, &quot;160067777401491&quot;);
      chatbox.setAttribute(&quot;attribution&quot;, &quot;biz_inbox&quot;);
    

    
    
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v12.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    
    
	
    
    			
                
                
                	© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.
                
                
            
    
    
    


/html[1]/body[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//body[(text() = concat(&quot;


    

        
        
            
                
                    Toggle navigation
                    
                    
                    
                
                
            
            

            
                
				
                
                
              
          
            

            
                
                    
                    	                        
                   	 	
                        
                             ลงทะเบียนครั้งแรก      First Register
                        
                        
                        
                             เปลี่ยนรหัสผ่าน/ต่ออายุรหัสผ่าน      Change password/Renew password
                        
                        
                             กู้คืนรหัสผ่าน/ต่ออายุรหัสผ่าน      Recovery password/Renew password
                        
                        
                             เข้าสู่ระบบ
                                  Sign In
                        
						
                             ติดต่อเรา      Contact Us
                        
                        
                        
                        
                             คู่มือการใช้งาน      Manual
                        
                        
                             คู่มือ Digital Cert for Windows      
                        
                        
                             คู่มือ Digital Cert for Mac      
                        
                                            
                
                
            
            
        

        
            
                
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                    Change Password
                
            
        
    
    



function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  var z = document.getElementById(&quot;oldpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
	z.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
	z.type = &quot;password&quot;;
  }
}

function checkuser(event){
	var ew = event.which;
	if(ew == 45)
        return true;
	if(ew == 46)
        return true;
	if(ew == 32)
        return true;
    if(48 &lt;= ew &amp;&amp; ew &lt;= 57)
        return true;
    if(65 &lt;= ew &amp;&amp; ew &lt;= 90)
        return true;
    if(97 &lt;= ew &amp;&amp; ew &lt;= 122)
        return true;
    return false;
}

function checkold(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don&quot; , &quot;'&quot; , &quot;t use &quot; , &quot;'&quot; , &quot; (single quote) ห้ามใช้เครื่องหมาย &quot; , &quot;'&quot; , &quot; (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                
                
                
            
        
        
    
    
    

    
    

    
      var chatbox = document.getElementById(&quot; , &quot;'&quot; , &quot;fb-customer-chat&quot; , &quot;'&quot; , &quot;);
      chatbox.setAttribute(&quot;page_id&quot;, &quot;160067777401491&quot;);
      chatbox.setAttribute(&quot;attribution&quot;, &quot;biz_inbox&quot;);
    

    
    
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : &quot; , &quot;'&quot; , &quot;v12.0&quot; , &quot;'&quot; , &quot;
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = &quot; , &quot;'&quot; , &quot;https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js&quot; , &quot;'&quot; , &quot;;
        fjs.parentNode.insertBefore(js, fjs);
      }(document, &quot; , &quot;'&quot; , &quot;script&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;facebook-jssdk&quot; , &quot;'&quot; , &quot;));
    
    
	
    
    			
                
                
                	© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.
                
                
            
    
    
    


/html[1]/body[1]&quot;) or . = concat(&quot;


    

        
        
            
                
                    Toggle navigation
                    
                    
                    
                
                
            
            

            
                
				
                
                
              
          
            

            
                
                    
                    	                        
                   	 	
                        
                             ลงทะเบียนครั้งแรก      First Register
                        
                        
                        
                             เปลี่ยนรหัสผ่าน/ต่ออายุรหัสผ่าน      Change password/Renew password
                        
                        
                             กู้คืนรหัสผ่าน/ต่ออายุรหัสผ่าน      Recovery password/Renew password
                        
                        
                             เข้าสู่ระบบ
                                  Sign In
                        
						
                             ติดต่อเรา      Contact Us
                        
                        
                        
                        
                             คู่มือการใช้งาน      Manual
                        
                        
                             คู่มือ Digital Cert for Windows      
                        
                        
                             คู่มือ Digital Cert for Mac      
                        
                                            
                
                
            
            
        

        
            
                
                	
/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: &quot;✔&quot;;
}

/* Add a red text color and an &quot;x&quot; when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: &quot;✖&quot;;
}

เปลี่ยนรหัสผ่าน (Change Password)

        
        
            
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                    Change Password
                
            
        
    
    



function myFunction() {
  var x = document.getElementById(&quot;newpass&quot;);
  var y = document.getElementById(&quot;renewpass&quot;);
  var z = document.getElementById(&quot;oldpass&quot;);
  if (x.type === &quot;password&quot;) {
    x.type = &quot;text&quot;;
	y.type = &quot;text&quot;;
	z.type = &quot;text&quot;;
  } else {
    x.type = &quot;password&quot;;
	y.type = &quot;password&quot;;
	z.type = &quot;password&quot;;
  }
}

function checkuser(event){
	var ew = event.which;
	if(ew == 45)
        return true;
	if(ew == 46)
        return true;
	if(ew == 32)
        return true;
    if(48 &lt;= ew &amp;&amp; ew &lt;= 57)
        return true;
    if(65 &lt;= ew &amp;&amp; ew &lt;= 90)
        return true;
    if(97 &lt;= ew &amp;&amp; ew &lt;= 122)
        return true;
    return false;
}

function checkold(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

function check(event){
	var ew = event.which;
	
    if(33 &lt;= ew &amp;&amp; ew &lt;= 38)
        return true;
    if(40 &lt;= ew &amp;&amp; ew &lt;= 126)
        return true;
    if(ew == 39){
		alert(&quot;Don&quot; , &quot;'&quot; , &quot;t use &quot; , &quot;'&quot; , &quot; (single quote) ห้ามใช้เครื่องหมาย &quot; , &quot;'&quot; , &quot; (เขาเดี่ยว)&quot;);
        return false;
	}
	alert(&quot;กรุณาเปลี่ยนภาษา&quot;);
    return false;
}

var myInput = document.getElementById(&quot;newpass&quot;);
var letter = document.getElementById(&quot;letter&quot;);
var capital = document.getElementById(&quot;capital&quot;);
var number = document.getElementById(&quot;number&quot;);
var length = document.getElementById(&quot;length&quot;);

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;block&quot;;
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById(&quot;message&quot;).style.display = &quot;none&quot;;
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-zA-Z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove(&quot;invalid&quot;);
    letter.classList.add(&quot;valid&quot;);
  } else {
    letter.classList.remove(&quot;valid&quot;);
    letter.classList.add(&quot;invalid&quot;);
  }
  
  // Validate capital letters
  var upperCaseLetters = /[\/\:\;\(\)\$\&amp;\@\&quot;\.\,\?\!\[\]\{\}\#\%\^\*\+\-\_\\\|\~\&lt;\> ]{1,}/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove(&quot;invalid&quot;);
    capital.classList.add(&quot;valid&quot;);
  } else {
    capital.classList.remove(&quot;valid&quot;);
    capital.classList.add(&quot;invalid&quot;);
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove(&quot;invalid&quot;);
    number.classList.add(&quot;valid&quot;);
  } else {
    number.classList.remove(&quot;valid&quot;);
    number.classList.add(&quot;invalid&quot;);
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove(&quot;invalid&quot;);
    length.classList.add(&quot;valid&quot;);
  } else {
    length.classList.remove(&quot;valid&quot;);
    length.classList.add(&quot;invalid&quot;);
  }
}

                    
                    
                
                
                
            
        
        
    
    
    

    
    

    
      var chatbox = document.getElementById(&quot; , &quot;'&quot; , &quot;fb-customer-chat&quot; , &quot;'&quot; , &quot;);
      chatbox.setAttribute(&quot;page_id&quot;, &quot;160067777401491&quot;);
      chatbox.setAttribute(&quot;attribution&quot;, &quot;biz_inbox&quot;);
    

    
    
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : &quot; , &quot;'&quot; , &quot;v12.0&quot; , &quot;'&quot; , &quot;
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = &quot; , &quot;'&quot; , &quot;https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js&quot; , &quot;'&quot; , &quot;;
        fjs.parentNode.insertBefore(js, fjs);
      }(document, &quot; , &quot;'&quot; , &quot;script&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;facebook-jssdk&quot; , &quot;'&quot; , &quot;));
    
    
	
    
    			
                
                
                	© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.
                
                
            
    
    
    


/html[1]/body[1]&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
